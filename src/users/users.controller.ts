import { Controller,
    Get,
    Param,
    Post,
    Body,
    Patch,
    Delete,
    Query,
    Inject,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { PaginationQueryDto } from 'src/common/dto/pagination-query.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
@Controller('users')
export class UsersController {
    constructor(
        private readonly usersService: UsersService,
      ) {}
    
    @Get()
    findAll(@Query() paginationQuery: PaginationQueryDto) {
      return this.usersService.findAll(paginationQuery);
    }

    @Get(':id')
    findOne(@Param('id') id: string) {
      return this.usersService.findOne(+id);
    }
  
    @Post()
    create(@Body() createUserDto: CreateUserDto) {
      return this.usersService.create(createUserDto);
    }

    @Post('developers')
    createDeveloper(@Body() createUserDto: CreateUserDto) {
      return this.usersService.createDeveloper(createUserDto);
    }

    @Post('managers')
    createManager(@Body() createUserDto: CreateUserDto) {
      return this.usersService.createManager(createUserDto);
    }
  
    @Patch(':id')
    update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
      return this.usersService.update(+id, updateUserDto);
    }
  
    @Delete(`:id`)
    remove(@Param('id') id: string) {
      return this.usersService.remove(+id);
    }
}