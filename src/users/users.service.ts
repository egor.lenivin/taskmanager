import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { ConfigService } from '@nestjs/config';
import { PaginationQueryDto } from 'src/common/dto/pagination-query.dto';
import { User } from './entities/user.entity';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Developer } from './entities/developer.entity';
import { Manager } from './entities/manager.entity';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
        @InjectRepository(Developer)
        private readonly developerRepository: Repository<Developer>,
        @InjectRepository(Manager)
        private readonly managerRepository: Repository<Manager>,
        private readonly configService: ConfigService,
      ) {}

    findAll(paginationQuery: PaginationQueryDto) {
        const { limit, offset } = paginationQuery;
        return this.userRepository.find({
          skip: offset,
          take: limit,
        });
      }

      async findOne(id: number) {
        const user = await this.userRepository.findOne(id);
        if (!user) {
          throw new NotFoundException(`User #${id} not found`);
        }
        return user;
      }
    
      async create(createUserDto: CreateUserDto) {
        const user = this.userRepository.create(createUserDto);
        return this.userRepository.save(user);
      }

      async createDeveloper(createUserDto: CreateUserDto) {
        const developer = this.developerRepository.create({
          ...createUserDto,
        });
        return this.developerRepository.save(developer);
      }

      async createManager(createUserDto: CreateUserDto) {
        const manager = this.managerRepository.create({
          ...createUserDto,
        });
        return this.managerRepository.save(manager);
      }
  
      async update(id: number, updateUserDto: UpdateUserDto) {  
        const user = await this.userRepository.preload({
          // preload finds entity if it exists
          id: id,
          ...updateUserDto,
        });
        if (!user) {
          throw new NotFoundException(`User #${id} not found`);
        }
        return this.userRepository.save(user);
      }
    
      async remove(id: number) {
        const user = await this.findOne(id);
        return this.userRepository.remove(user);
      }
}
