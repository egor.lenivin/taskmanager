import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    BeforeInsert,
    TableInheritance
  } from 'typeorm';
import { IsEmail } from 'class-validator';
import * as argon2 from 'argon2';

@Entity()
@TableInheritance({ column: { type: "varchar"} })
// в примере так:
// @TableInheritance({ column: { type: "varchar", name: "type" } })
// нужно ли указывать name? 
// "By default the column name is generated from the name of the property"
export class User {
  @PrimaryGeneratedColumn()
  id: number;
  
  @Column()
  first_name: string;
  
  @Column({ nullable: true })
  last_name: string;

  @Column()
  @IsEmail()
  email: string;
  
  @Column({default: ''})
  avatar: string;

  @Column()
  password: string;

  @BeforeInsert()
  async hashPassword() {
    this.password = await argon2.hash(this.password);
  }
}