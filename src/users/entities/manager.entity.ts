import {
    ChildEntity,
    OneToMany,
  } from 'typeorm';

import { User } from './user.entity';
import { Task } from 'src/tasks/entities/task.entity';

@ChildEntity()
export class Manager extends User {
  @OneToMany(type => Task, task => task.author)
  created_tasks: Task[];
}