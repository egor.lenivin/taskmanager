import {
    ChildEntity,
    OneToMany,
  } from 'typeorm';

import { User } from './user.entity';
import { Task } from 'src/tasks/entities/task.entity';

@ChildEntity()
export class Developer extends User {
  @OneToMany(type => Task, task => task.assignee)
  assigned_tasks: Task[];
}