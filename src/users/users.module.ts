import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { User } from './entities/user.entity';
import { Developer } from './entities/developer.entity';
import { Manager } from './entities/manager.entity';
import { Admin } from './entities/admin.entity';
import { AdminsController } from './admins/admins.controller';

@Module({
  imports: [TypeOrmModule.forFeature([User, Developer, Manager, Admin]), ConfigModule],
  controllers: [UsersController, AdminsController],
  providers: [UsersService]
})
export class UsersModule {}
