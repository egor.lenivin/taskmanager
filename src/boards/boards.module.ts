import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { RolesGuard } from 'src/guards/roles.guard';
import { BoardsController } from './boards.controller';

@Module({
    providers: [RolesGuard],
    controllers: [BoardsController]
})
export class BoardsModule {}
