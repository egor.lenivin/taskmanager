import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { TasksController } from './tasks.controller';
import { TasksService } from './tasks.service';
import { Task } from './entities/task.entity';
import { User } from 'src/users/entities/user.entity';
import { Admin } from 'src/users/entities/admin.entity';
import { Manager } from 'src/users/entities/manager.entity';
import { Developer } from 'src/users/entities/developer.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Task, Admin, Developer, Manager]), ConfigModule],
  controllers: [TasksController],
  providers: [TasksService]
})
export class TasksModule {}
