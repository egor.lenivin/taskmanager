import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Connection, Repository } from 'typeorm';
import { ConfigService } from '@nestjs/config';
import { PaginationQueryDto } from 'src/common/dto/pagination-query.dto';
import { Task } from './entities/task.entity';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
@Injectable()
export class TasksService {
    constructor(
        @InjectRepository(Task)
        private readonly taskRepository: Repository<Task>,
        private readonly configService: ConfigService,
      ) {}

    findAll(paginationQuery: PaginationQueryDto) {
        const { limit, offset } = paginationQuery;
        return this.taskRepository.find({
          skip: offset,
          take: limit,
        });
      }

      async findOne(id: number) {
        const task = await this.taskRepository.findOne(id);
        if (!task) {
          throw new NotFoundException(`Task #${id} not found`);
        }
        return task;
      }
    
      async create(createTaskDto: CreateTaskDto) {
        const task = this.taskRepository.create({
          ...createTaskDto,
        });
        return this.taskRepository.save(task);
      }
  
      async update(id: number, updateTaskDto: UpdateTaskDto) {  
        const task = await this.taskRepository.preload({
          // preload finds entity if it exists
          id: id,
          ...updateTaskDto,
        });
        if (!task) {
          throw new NotFoundException(`Task #${id} not found`);
        }
        return this.taskRepository.save(task);
      }
    
      async remove(id: number) {
        const task = await this.findOne(id);
        return this.taskRepository.remove(task);
      }
}
