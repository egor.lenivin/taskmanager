import {
    Column,
    Entity,
    PrimaryGeneratedColumn,
    ManyToOne,
    JoinColumn
  } from 'typeorm';
  import { User } from 'src/users/entities/user.entity';
import { Manager } from 'src/users/entities/manager.entity';
import { Developer } from 'src/users/entities/developer.entity';

  @Entity()
  export class Task {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column()
  state: string;

  @Column({default: ''})
  image: string;

  @Column()
  expired_at: number;

  @ManyToOne(() => Manager, author => author.created_tasks)
  @JoinColumn({ name: "author_id" })
  author: Manager;

  @ManyToOne(() => Developer, assignee => assignee.assigned_tasks)
  @JoinColumn({ name: "assignee_id" })
  assignee: Developer;
}
